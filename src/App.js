import React, { Component } from 'react';

import Footer from './layouts/Footer';

import Login from './pages/Login';
import Home from './pages/Home';
import About from './pages/About';

import { Switch, Route } from 'react-router-dom';

class App extends Component {
    
  render() {
   
    return (
      <div>
        <main>
          <Switch>
            <Route exact path='/' component={Login} />
            <Route path='/home' component={Home} />
            <Route  path='/about' component={About} >
           </Route>
          </Switch>
        </main>
        <Footer />
      </div>
    );
  }
}

export default App;
