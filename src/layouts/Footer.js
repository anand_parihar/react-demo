import React, { Component } from 'react';

class Footer extends Component {
  render(){
    return(
      <div>
        <footer>
            <div className="container">
                <div className="row">
                    <div className="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                        <p className="copyright text-muted">Copyright &copy; Your Website 2016</p>
                    </div>
                </div>
            </div>
        </footer>
      </div>
    );
  }
}

export default Footer;
