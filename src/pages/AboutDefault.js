import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import Contact from './Contact';
import Profile from './Profile';
import Team from './Team';


class AboutDefault extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.isLogin = this.isLogin.bind(this);
    }

    isLogin() {
        return true;
    }

    render() {

        if (!this.isLogin()) {
            return <Redirect to="/"/>
        } else {
            return(
                    <div>
                        <div className="container">
                            <Profile/>
                            <Contact/>
                            <Team/>
                        </div>
                        <hr />
                    </div>
                    );
        }
    }
}

export default AboutDefault;
