import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import HelpFunction from '../helpers/HelpFunction';


class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fields: {'email': '', 'password': ''},
            errors: {}
        }
        this.isLogin = this.isLogin.bind(this);
    }

    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        //Email
        if (!fields["email"]) {
            formIsValid = false;
            errors["email"] = "Please enter Email!";
        } else {
            if (typeof fields["email"] !== "undefined") {
                let lastAtPos = fields["email"].lastIndexOf('@');
                let lastDotPos = fields["email"].lastIndexOf('.');

                if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
                    formIsValid = false;
                    errors["email"] = "Email is not valid";
                }
            }
        }

        //Password
        if (!fields["password"]) {
            formIsValid = false;
            errors["password"] = "Please enter Password!";
        }

        this.setState({errors: errors});
        return formIsValid;
    }

    loginSubmit(e) {
        e.preventDefault();
        if (this.handleValidation()) {
            fetch(window.location.origin + '/auth.json').then(response => response.json()).then(json => {
                if (this.state.fields["email"] === json.user && this.state.fields["password"] === json.password) {
                    HelpFunction.setCookie("isLogin", "true", 5);
                    this.forceUpdate();
                } else {
                    let errors = {};
                    errors["password"] = "Invalid user name or password!!";
                    this.setState({errors: errors});
                }
            })
        }
    }

    handleChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({fields});
    }

    isLogin() {
        if (HelpFunction.getCookie("isLogin")) {
            return true;
        } else {
            return false;
        }
    }

    render() {
        if (this.isLogin()) {
            return <Redirect to="/home"/>
        } else {
            return(
                    <div>
                        <header className="intro-header home-header-bg">
                            <div className="container">
                                <div className="row">
                                    <div className="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                                        <div className="site-heading">
                                            <h1>Login</h1>
                                            <hr className="small" />
                                            <span className="subheading">Your company is awesome!!</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </header>
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                                    <form name="contactform" className="contactform" onSubmit= {this.loginSubmit.bind(this)}>
                                        <div className="row control-group">
                                            <div className="form-group col-xs-12 floating-label-form-group controls">
                                                <label>Email Address</label>
                                                <input className="form-control" type="text" placeholder="Email Address" onChange={this.handleChange.bind(this, "email")} value={this.state.fields["email"]}/>
                                                <p className="text-danger">{this.state.errors["email"]}</p>
                                            </div>
                                        </div>
                                        <div className="row control-group">
                                            <div className="form-group col-xs-12 floating-label-form-group controls">
                                                <label>Password</label>
                                                <input type="password" className="form-control" placeholder="Password" onChange={this.handleChange.bind(this, "password")} value={this.state.fields["password"]}/>
                                                <p className="text-danger">{this.state.errors["password"]}</p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="form-group col-xs-12">
                                                <button type="submit" id="submit" className="btn btn-default">Login</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <hr />
                    </div>
                    );
        }
    }
}

export default Login;
