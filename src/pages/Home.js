import React, { Component } from 'react';

import HelpFunction from '../helpers/HelpFunction';
import { Redirect, Link } from 'react-router-dom';

class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {};
        this.isLogin = this.isLogin.bind(this);
        this.logout = this.logout.bind(this);
    }

    isLogin() {
        if (HelpFunction.getCookie("isLogin")) {
            return true;
        } else {
            return false;
        }
    }

    logout() {
        HelpFunction.deleteCookie("isLogin");
        this.forceUpdate();
    }

    render() {

        if (!this.isLogin()) {
            return <Redirect to="/"/>
        } else {

            return(
                    <div>
                        <header className="intro-header home-header-bg">
                            <div className="container">
                                <div className="row">
                                    <div className="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                                        <div className="site-heading">
                                            <h1>Home </h1>
                                            <hr className="small" />
                                            <span className="subheading">Explore about us for more information</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </header>
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3  col-sm-12">
                                    <ul className="pager">
                                        <li className="next">
                                            <a  onClick= {this.logout.bind(this)}>Logout &rarr;</a>
                                        </li>
                                        <li className="next">
                                            <Link to="/about">Go To About Us &rarr;</Link>
                                        </li>                    
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <hr />
                    </div>
                    );
        }
    }
}

export default Home;
