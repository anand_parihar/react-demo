import React, { Component } from 'react';
import { Redirect,Link,Route } from 'react-router-dom';
import AboutDefault from './AboutDefault';
import Contact from './Contact';
import Profile from './Profile';
import Team from './Team';
import HelpFunction from '../helpers/HelpFunction';


class About extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.isLogin = this.isLogin.bind(this);
        this.logout = this.logout.bind(this);
    }

     isLogin() {
        if(HelpFunction.getCookie("isLogin")){
          return true;        
        }else{
            return false;
        }
    }
    
    logout() {
        HelpFunction.deleteCookie("isLogin");
        this.forceUpdate();
    }

    render() {

        if (!this.isLogin()) {
            return <Redirect to="/"/>
        } else {
            return(
                    <div>
                        <header className="intro-header about-header-bg">
                            <div className="container">
                                <div className="row">
                                    <div className="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                                        <div className="page-heading">
                                            <h1>About Me</h1>
                                            <hr className="small" />
                                            <span className="subheading">This is what I do.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </header>
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3">
                                    <ul className="nav navbar-nav ">
                                        <li><Link  to="/about">About</Link></li>
                                        <li><Link  to="/about/profile">Profile</Link></li>
                                        <li><Link  to="/about/contact">Contact Us</Link></li>
                                        <li><Link  to="/about/team">Team</Link></li>
                                        <li><a  onClick= {this.logout.bind(this)}>Logout &rarr;</a></li>
                                    </ul>
                                </div>
                            </div>
                            
                             <Route exact path={this.props.match.path} component={AboutDefault}/>
                             <Route path={`${this.props.match.path}/contact` } component={Contact}/>
                             <Route path={`${this.props.match.path}/profile` } component={Profile}/>
                              <Route path={`${this.props.match.path}/team` } component={Team}/>
                            
                        </div>
                        <hr />
                    </div>
                    );
        }
    }
}

export default About;
