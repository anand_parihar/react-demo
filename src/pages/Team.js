import React, { Component } from 'react';

class Team extends Component {
    render() {
        return(
                <div>
                    <header className="intro-header about-header-bg">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                                    <div className="page-heading">
                                        <h1>Team</h1>
                                        <hr className="small" />
                                        <span className="subheading">This is what I do.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </header>
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                
                            </div>
                        </div>
                    </div>
                    <hr />
                </div>
                );

    }
}

export default Team;
